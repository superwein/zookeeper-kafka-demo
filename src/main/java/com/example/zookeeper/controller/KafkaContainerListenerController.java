package com.example.zookeeper.controller;

import com.example.zookeeper.config.kafka.KafkaContainerListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaContainerListenerController {

    @Autowired
    private KafkaContainerListener kafkaContainerListener;

    /**
     * 消费开关 停止消费
     */
    @RequestMapping("/{containerId}/stop")
    public void stop(@PathVariable String containerId) {
        kafkaContainerListener.stopListener(containerId);
    }

    /**
     * 消费开关 开始消费
     */
    @RequestMapping("/{containerId}/start")
    public void start(@PathVariable String containerId) {
        kafkaContainerListener.startListener(containerId);
    }

}
