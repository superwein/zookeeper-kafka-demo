package com.example.zookeeper.actuator;

import org.springframework.boot.actuate.endpoint.annotation.DeleteOperation;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义端点
 */
@Component
@Endpoint(id = "memery")
public class MemoryEndpoint {

    @ReadOperation
    public Map<String, Object> getHello() {
        Runtime runtime = Runtime.getRuntime();
        Long maxMemory = runtime.maxMemory();
        Long totalMemory = runtime.totalMemory();
        Map<String, Object> memoryMap = new HashMap<String, Object>(2, 1);
        memoryMap.put("maxMemory", maxMemory);
        memoryMap.put("totalMemory", totalMemory);
        return memoryMap;
    }

    @WriteOperation
    public String postHello() {
        return "post Hello";
    }

    @DeleteOperation
    public String deleteHello() {
        return "delete Hello";
    }

}
