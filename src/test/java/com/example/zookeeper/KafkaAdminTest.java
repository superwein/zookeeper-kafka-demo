package com.example.zookeeper;

import com.alibaba.fastjson.JSON;
import com.example.zookeeper.util.KafkaAdminUtil;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.ExecutionException;

/**
 * kafka admin 测试类
 */
@SpringBootTest(classes = ZookeeperApplication.class)
public class KafkaAdminTest {

    @Autowired
    private KafkaAdminUtil kafkaAdminUtil;

    @Test
    public void createTopicTest() throws InterruptedException {
        // 发送消息offset分区  topic名字取hashcode，除以50取余加1
        // System.out.println(Math.abs("replication_topic".hashCode())%50 + 1);
        CreateTopicsResult result = kafkaAdminUtil.createTopic("test2", 5, (short) 3);
        System.out.println(JSON.toJSONString(result));
    }

    @Test
    public void deleteTopicTest() throws InterruptedException {
        DeleteTopicsResult result = kafkaAdminUtil.deleteTopic("test");
        System.out.println(JSON.toJSONString(result));
    }

    @Test
    public void getTopicInfoTest() throws InterruptedException, ExecutionException {
        DescribeTopicsResult result = kafkaAdminUtil.getTopicInfo("replication_topic");
        System.out.println(JSON.toJSONString(result));
    }

    @Test
    public void describeTopicConfigTest() throws InterruptedException, ExecutionException {
        kafkaAdminUtil.describeTopicConfig("replication_topic");
    }

    @Test
    public void describeClusterTest() throws InterruptedException, ExecutionException {
        kafkaAdminUtil.describeTopicConfig("replication_topic");
    }

}
