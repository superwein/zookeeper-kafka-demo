package com.example.zookeeper;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.data.Stat;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import javax.sound.midi.Soundbank;

/**
 * zookeeper客户端curator测试类
 */
//@SpringBootTest
class ZookeeperApplicationTests {

    @Test
    public void availableProcessors() {
        System.out.println(Runtime.getRuntime().availableProcessors());
    }

    @Test
    public void zookeeperTest() throws Exception {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .connectString("192.168.85.128:2181")
                .retryPolicy(retryPolicy)
                .sessionTimeoutMs(6000)
                .connectionTimeoutMs(6000)
                .build();
        client.start();
        boolean isZkCuratorStarted = client.isStarted();
        System.out.println("当前客户端的状态：" + (isZkCuratorStarted ? "连接中..." : "已关闭..."));
        // 创建持久化节点
        // client.create().forPath("/zktest2", "123456".getBytes());
        // 创建临时节点，会话断开后，zookeeper server会自动删除临时节点
        //client.create().withMode(CreateMode.EPHEMERAL).forPath("/tmp_node", "11111".getBytes());
        // 创建一个节点，指定创建模式（临时节点），附带初始化内容，并且自动递归创建父节点
        //client.create().creatingParentContainersIfNeeded().withMode(CreateMode.EPHEMERAL).forPath("/tmp_nodes/node1","init".getBytes());
        // 删除节点
        // client.delete().forPath("/zktest2");
        // 删除指定版本
        // client.delete().withVersion(1).forPath("/zktest");
        // 删除一个节点，保证删除
        // client.delete().guaranteed().forPath("path");
        // 删除一个节点，并且递归删除其所有的子节点
        // client.delete().deletingChildrenIfNeeded().forPath("/zktest");
        // 更新节点数据
        client.setData().forPath("/zktest2","data".getBytes());
        //client.setData().withVersion(10086).forPath("path","data".getBytes());
        // 查看节点是否存在
        //System.out.println(client.checkExists().forPath("/zktest2"));
        // 获取节点数据
        byte[] data = client.getData().usingWatcher(new MyCuratorWatcher(client)).forPath("/zktest2");
        System.out.println(new String(data, "utf-8"));
        // 获取节点数据，同时返回该节点的stat
        //Stat stat = new Stat();
        //System.out.println(new String(client.getData().storingStatIn(stat).forPath("/zktest2"), "utf-8"));
        //System.out.println(stat);
        // 事务
//        client.inTransaction().check().forPath("/path")
//                .and()
//                .create().withMode(CreateMode.EPHEMERAL).forPath("/path","data".getBytes())
//                .and()
//                .setData().withVersion(10086).forPath("/path","data2".getBytes())
//                .and()
//                .commit();
        // client.close();
        // 获取当前客户端的状态
        isZkCuratorStarted = client.isStarted();
        System.out.println("当前客户端的状态：" + (isZkCuratorStarted ? "连接中..." : "已关闭..."));
        System.in.read();
    }


    public class MyCuratorWatcher implements CuratorWatcher {
        private CuratorFramework client;

        public MyCuratorWatcher(CuratorFramework client) {
            this.client = client;
        }

        // Watcher事件通知方法
        public void process(WatchedEvent watchedEvent) throws Exception {
            byte[] data = client.getData().usingWatcher(this).forPath(watchedEvent.getPath());
            System.out.println("触发watcher，节点路径为：" + watchedEvent.getPath() + "，节点数据：" + new String(data, "utf-8"));
        }
    }

}
