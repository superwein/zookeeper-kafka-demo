package com.example.zookeeper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.zookeeper.common.constant.Contants;
import com.example.zookeeper.config.kafka.KafkaSender;
import com.example.zookeeper.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

/**
 * kafka 消息发送测试类
 */
@SpringBootTest(classes = ZookeeperApplication.class)
public class KafkaMsgSendingTest {

    @Value("#{'${kafka.listener.topics}'.split(',')}")
    private List<String> topics;

    @Autowired
    private KafkaSender kafkaSender;

    //@Scheduled(fixedRate = 10 * 1000)
    @Test
    public void sendMsgTest() {
        User user;
        for (int i = 0; i < 10; i++) {
            user = new User();
            user.setUserName("HS" + i);
            user.setDescription("text");
            user.setCreateTime(LocalDateTime.now());
            String JSONUser = JSON.toJSONStringWithDateFormat(user,
                    Contants.DateTimeFormat.DATE_TIME_PATTERN, //日期格式化
                    SerializerFeature.PrettyFormat); //格式化json
            kafkaSender.sendMessage(topics.get(0), String.valueOf(i), JSONUser);
        }
    }

}
